# tensorflow-cuda-3.0-docker

Contains the Dockerfile (and deploy scripts) for building a Docker image configured with TensorFlow for CUDA Capability 3.0 GPUs.